# Tekst zadatka

Implementirajte ovaj scenarij:

Marko, markantan mladić od 23 godine, visine 180 i smeđih očiju, odlučio je otići s prijateljima na piće. S obzirom da je kafić daleko, odlučio je odvesti se autom.
Marko inače vozi BMWa koji mu je parkiran i ugašen ispred kuće.
Radnje koje Marko mora napraviti da bi popio toliko željeno piće:

- Otključati auto
- Otvoriti vrata auta
- Sjesti u auto
- Zatvoriti vrata
- Upaliti auto
- Odvesti se do kafića
- Ugasiti auto
- Otvoriti vrata
- Izaći iz auta
- Zatvoriti vrata
- Zaključati auto



Jedan od Markovih prijatelja, Ante, ostao je bez prijevoza.
Ante je mladić 22 godine, visine 175 i plavih očiju. Marko je odlučio odvesti Antu kući.
Radnje koje mora napraviti Ante:

- Otvoriti vrata auta
- Sjesti u auto
- Zatvoriti vrata



Radnje koje mora napraviti Marko:

- Otključati auto
- Otvoriti vrata auta
- Sjesti u auto
- Zatvoriti vrata
- Upaliti auto
- Odvesti Antu kući
- Odvesti sebe kući
- Ugasiti auto
- Otvoriti vrata
- Izaći iz auta
- Zatvoriti vrata
- Zaključati auto